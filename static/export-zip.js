
require([], function() {

    $(function() {

        var action = $('.chart-actions .action-export-zip');

        $('a', action).click(function(e) {
            e.preventDefault();
            showModal();
        });

        function showModal() {
            $.get('/plugins/export-zip/export-zip.twig', function(data) {
                var modal = $('<div class="publish-chart-action-modal modal hide">' + data + '</div>').modal();
                var url   = '/export-zip/zipper/' + dw.backend.currentChart.get('id');

                $('.btn-export-zip', modal).attr('href', url);
            });
        }
    });

});
