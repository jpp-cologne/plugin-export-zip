<?php

class DatawrapperPlugin_ExportZip extends DatawrapperPlugin {

    public function init(){
        $plugin = $this;

        // hook into chart publication
        DatawrapperHooks::register(DatawrapperHooks::GET_CHART_ACTIONS, function() use ($plugin) {
            // no export possible without email
            $cfg = $plugin->getConfig();
            return array(
                'id' => 'export-zip',
                'title' => __('publish / button-label', $plugin->getName()),
                'icon' => 'download',
                'order' => 400
            );
        });

        $this->declareAssets(array(
            'export-zip.js',
            'export-zip.css'
        ),
        "|/chart/[^/]+/publish|");

        $this->registerController($this, 'zipper');
    }

    public function zipper($app) {
        $app->get('/export-zip/zipper/:chart_id', function($chartID) use ($app) {
            check_chart_readable($chartID, function(User $user, Chart $chart) use ($app) {
                global $dw_config;

                require_once __DIR__.DIRECTORY_SEPARATOR.'Zipper.php';

                $zipper  = new DatawrapperPlugin_ExportZip_Zipper($dw_config);
                $archive = $zipper->createZip($user, $chart);

                // send the archive to the client
                $response = $app->response();

                $response->header('content-type', 'application/zip');
                $response->header('content-disposition', 'attachment; filename="export.zip"');

                readfile($archive);
                unlink($archive);
            });
        });
    }
}
