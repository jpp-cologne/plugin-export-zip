<?php

class DatawrapperPlugin_ExportZip_Zipper {
    protected $domain;
    protected $chartDomain;
    protected $assetDomain;

    public function __construct(array $config) {
        $this->domain      = $config['domain'];
        $this->chartDomain = $config['chart_domain'];
        $this->assetDomain = $config['asset_domain'];
    }

    public function createZip(User $user, Chart $chart) {
        // (re-)publish chart if neccessary
        if ($chart->getPublishedAt() < $chart->getLastModifiedAt()) {
            $chart->publish();
            publish_chart($user, $chart, false, true);
        }

        $chartID = $chart->getId();
        $rootDir = $this->getChartPublishDir() . 'static/' . $chartID . '/';
        $tmpFile = tempnam(sys_get_temp_dir(), 'dwzip');
        $queue   = array();
        $archive = new ZipArchive();

        // scan the chart's index.html for embedded resources
        $index = file_get_contents($rootDir.'index.html');

        // remove Piwik tracking code
        $index = preg_replace('/<\!-- Piwik -->.*?<\!-- End Piwik Code -->/si', '', $index);

        // extract all embedded links
        preg_match_all('/ (href|src)=(["\'])(.*?)\2/', $index, $matches);

        foreach ($matches[3] as $url) {
            $normalized = $this->normalize($url, $chartID);

            if ($this->isLocal($normalized)) {
                $queue[$this->mapUrlToFile($normalized)][] = $url;
            }
        }

        // collect theme files
        $theme = DatawrapperTheme::get($chart->getTheme());

        if (!empty($theme['assets'])) {
            foreach ($theme['assets'] as $filename) {
                $normalized           = $this->mapUrlToFile($this->normalize($filename, $chartID));
                $queue[$normalized][] = $filename;
            }
        }

        // download assets, postprocess them and stuff them in the archive
        $archive->open($tmpFile, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($queue as $normalizedUrl => $originalUrls) {
            // skip oembed links, because they won't work on non-public charts anway;
            // this is easier than breaking up the oembed plugin
            if (strpos($normalizedUrl, 'plugin/oembed') !== false) continue;

            $content = file_get_contents($normalizedUrl);

            $basename  = basename($normalizedUrl);
            $extension = pathinfo($basename, PATHINFO_EXTENSION);

            // turn "foo.js" into "js/foo.js"
            if ($extension === 'css' || $extension === 'js') {
                $basename = $extension . '/' . $basename;
            }

            // similarly for images
            if ($extension === 'png') {
                $basename = 'images/' . $basename;
            }

            if ($basename === 'data') {
                $basename = 'data.csv';
            }

            // replace all occurrances of this url in index.html
            foreach ($originalUrls as $url) {
                $index = str_replace('"' . $url . '"', '"' . $basename . '"', $index);
            }

            if ($basename) {
                $archive->addFromString($basename, $content);
            }
        }

        $archive->addFromString('index.html', $index);
        $archive->close();

        return $tmpFile;
    }

    protected function isLocal($url) {
        // skip links to the homepage
        if ($url == 'http://' . $this->domain . '/') return false;

        // skip oembed plugin
        if (strpos($url, 'plugin/oembed') !== false) return false;

        // expand // urls
        if (!preg_match('#^(https?:)?//([^/]+)#', $url, $match)) return true;

        return in_array($match[2], array($this->domain, $this->chartDomain, $this->assetDomain), true);
    }

    protected function normalize($url, $chartID) {
        if (substr($url, 0, 7) == 'http://') return $url;
        if (substr($url, 0, 8) == 'https://') return $url;
        if (substr($url, 0, 2) == '//') return 'http:' . $url;
        if (substr($url, 0, 1) == '/') return 'http://' . $this->chartDomain . $url;

        return 'http://' . $this->chartDomain . '/' . $chartID . '/' . $url;
    }

    protected function mapUrlToFile($url) {
        $parts = parse_url($url);
        $path  = ltrim($parts['path'], '/');

        // if there's a query string, fall back to going through HTTP to get the file
        if (strpos($url, '?') !== false) {
            return $url;
        }

        if ($parts['host'] === $this->chartDomain) {
            // fix old links still pointing to a data file, which is now called data.csv;
            // via HTTP this is taken care of by Apache's MultiView option
            if (substr($path, -5) === '/data') {
                $path .= '.csv';
            }

            return $this->getChartPublishDir() . 'static/' . $path;
        }
        elseif ($parts['host'] === $this->domain && substr($path, 0, 7) === 'static/') {
            return ROOT_PATH . 'www/' . $path;
        }

        return $url;
    }

    private function getChartPublishDir() {
        if (function_exists('chart_publish_directory')) { // dw 1.9.5+
            return chart_publish_directory();
        }
        else {
            return ROOT_PATH . 'charts/';
        }
    }
}
