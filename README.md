## Installation

```sh
> cd $DATAWRAPPER_HOME
# download the lastest plugin version as zip file 
> git archive --remote=ssh://git@bitbucket.org/jpp-cologne/plugin-export-zip.git --format=zip --output="export-zip.zip" master
# unzip the plugin into plugins folder
> unzip -d plugins/export-zip export-zip.zip
# install the plugin via command-line script
> php scripts/plugin.php install export-zip
Installed plugin export-zip.
```